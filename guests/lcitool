#!/usr/bin/env python

# lcitool - libvirt CI guest management tool
# Copyright (C) 2017-2018  Andrea Bolognani <abologna@redhat.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

import argparse
import distutils.spawn
import fnmatch
import json
import os
import platform
import random
import shutil
import string
import subprocess
import sys
import tempfile
import textwrap
import yaml

# This is necessary to maintain Python 2.7 compatibility
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser as ConfigParser


class Util:

    @staticmethod
    def get_base():
        return os.path.dirname(os.path.abspath(__file__))

    @staticmethod
    def mksalt():
        alphabeth = string.ascii_letters + string.digits
        salt = "".join(random.choice(alphabeth) for x in range(0, 16))
        return "$6${}$".format(salt)

    @staticmethod
    def expand_pattern(pattern, source, name):
        if pattern is None:
            raise Exception("Missing {} list".format(name))

        if pattern == "all":
            pattern = "*"

        # This works correctly for single items as well as more complex
        # cases such as explicit lists, glob patterns and any combination
        # of the above
        matches = []
        for partial_pattern in pattern.split(","):

            partial_matches = []
            for item in source:
                if fnmatch.fnmatch(item, partial_pattern):
                    partial_matches += [item]

            if not partial_matches:
                raise Exception("Invalid {} list '{}'".format(name, pattern))

            matches += partial_matches

        return sorted(set(matches))

    @staticmethod
    def get_native_arch():
        # Same canonicalization as libvirt virArchFromHost
        arch = platform.machine()
        if arch in ["i386", "i486", "i586"]:
            arch = "i686"
        if arch == "amd64":
            arch = "x86_64"
        return arch

    @staticmethod
    def native_arch_to_abi(native_arch):
        archmap = {
            "aarch64": "aarch64-linux-gnu",
            "armv6l": "arm-linux-gnueabi",
            "armv7l": "arm-linux-gnueabihf",
            "i686": "i686-linux-gnu",
            "mingw32": "i686-w64-mingw32",
            "mingw64": "x86_64-w64-mingw32",
            "mips": "mips-linux-gnu",
            "mipsel": "mipsel-linux-gnu",
            "mips64el": "mips64el-linux-gnuabi64",
            "ppc64le": "powerpc64le-linux-gnu",
            "s390x": "s390x-linux-gnu",
            "x86_64": "x86_64-linux-gnu",
        }
        if native_arch not in archmap:
            raise Exception("Unsupported architecture {}".format(native_arch))
        return archmap[native_arch]

    @staticmethod
    def native_arch_to_lib(native_arch):
        arch = Util.native_arch_to_abi(native_arch)
        if arch == "i686-linux-gnu":
            arch = "i386-linux-gnu"
        return arch

    @staticmethod
    def native_arch_to_deb_arch(native_arch):
        archmap = {
            "aarch64": "arm64",
            "armv6l": "armel",
            "armv7l": "armhf",
            "i686": "i386",
            "mips": "mips",
            "mipsel": "mipsel",
            "mips64el": "mips64el",
            "ppc64le": "ppc64el",
            "s390x": "s390x",
            "x86_64": "amd64",
        }
        if native_arch not in archmap:
            raise Exception("Unsupported architecture {}".format(native_arch))
        return archmap[native_arch]


class Config:

    @staticmethod
    def _get_config_file(name):
        try:
            config_dir = os.environ["XDG_CONFIG_HOME"]
        except KeyError:
            config_dir = os.path.join(os.environ["HOME"], ".config")
        config_dir = os.path.join(config_dir, "lcitool")

        # Create the directory if it doesn't already exist
        if not os.path.exists(config_dir):
            try:
                os.mkdir(config_dir)
            except Exception as ex:
                raise Exception(
                    "Can't create configuration directory ({}): {}".format(
                        config_dir, ex,
                    )
                )

        return os.path.join(config_dir, name)

    def get_flavor(self):
        flavor_file = self._get_config_file("flavor")

        try:
            with open(flavor_file, "r") as infile:
                flavor = infile.readline().strip()
        except Exception:
            # If the flavor has not been configured, we choose the default
            # and store it on disk to ensure consistent behavior from now on
            flavor = "test"
            try:
                with open(flavor_file, "w") as infile:
                    infile.write("{}\n".format(flavor))
            except Exception as ex:
                raise Exception(
                    "Can't write flavor file ({}): {}".format(
                        flavor_file, ex
                    )
                )

        if flavor not in ["test", "jenkins"]:
            raise Exception("Invalid flavor '{}'".format(flavor))

        return flavor

    def get_vault_password_file(self):
        vault_pass_file = None

        # The vault password is only needed for the jenkins flavor, but in
        # that case we want to make sure there's *something* in there
        if self.get_flavor() != "test":
            vault_pass_file = self._get_config_file("vault-password")

            try:
                with open(vault_pass_file, "r") as infile:
                    if not infile.readline().strip():
                        raise ValueError
            except Exception as ex:
                raise Exception(
                    "Missing or invalid vault password file ({}): {}".format(
                        vault_pass_file, ex
                    )
                )

        return vault_pass_file

    def get_root_password_file(self):
        root_pass_file = self._get_config_file("root-password")

        try:
            with open(root_pass_file, "r") as infile:
                if not infile.readline().strip():
                    raise ValueError
        except Exception as ex:
            raise Exception(
                "Missing or invalid root password file ({}): {}".format(
                    root_pass_file, ex
                )
            )

        return root_pass_file


class Inventory:

    def __init__(self):
        base = Util.get_base()
        ansible_cfg_path = os.path.join(base, "ansible.cfg")

        try:
            parser = ConfigParser()
            parser.read(ansible_cfg_path)
            inventory_path = parser.get("defaults", "inventory")
        except Exception as ex:
            raise Exception(
                "Can't read inventory location in ansible.cfg: {}".format(ex))

        inventory_path = os.path.join(base, inventory_path)

        self._facts = {}
        try:
            # We can only deal with trivial inventories, but that's
            # all we need right now and we can expand support further
            # later on if necessary
            with open(inventory_path, "r") as infile:
                for line in infile:
                    host = line.strip()
                    self._facts[host] = {}
        except Exception as ex:
            raise Exception(
                "Missing or invalid inventory ({}): {}".format(
                    inventory_path, ex
                )
            )

        for host in self._facts:
            try:
                self._facts[host] = self._read_all_facts(host)
                self._facts[host]["inventory_hostname"] = host
            except Exception as ex:
                raise Exception("Can't load facts for '{}': {}".format(
                    host, ex))

    @staticmethod
    def _add_facts_from_file(facts, yaml_path):
        with open(yaml_path, "r") as infile:
            some_facts = yaml.safe_load(infile)
            for fact in some_facts:
                facts[fact] = some_facts[fact]

    def _read_all_facts(self, host):
        base = Util.get_base()

        sources = [
            os.path.join(base, "group_vars", "all"),
            os.path.join(base, "host_vars", host),
        ]

        facts = {}

        # We load from group_vars/ first and host_vars/ second, sorting
        # files alphabetically; doing so should result in our view of
        # the facts matching Ansible's
        for source in sources:
            for item in sorted(os.listdir(source)):
                yaml_path = os.path.join(source, item)
                if not os.path.isfile(yaml_path):
                    continue
                if not yaml_path.endswith(".yml"):
                    continue
                self._add_facts_from_file(facts, yaml_path)

        return facts

    def expand_pattern(self, pattern):
        return Util.expand_pattern(pattern, self._facts, "host")

    def get_facts(self, host):
        return self._facts[host]


class Projects:

    def __init__(self):
        base = Util.get_base()

        mappings_path = os.path.join(base, "vars", "mappings.yml")

        try:
            with open(mappings_path, "r") as infile:
                mappings = yaml.safe_load(infile)
                self._mappings = mappings["mappings"]
                self._pip_mappings = mappings["pip_mappings"]
        except Exception as ex:
            raise Exception("Can't load mappings: {}".format(ex))

        source = os.path.join(base, "vars", "projects")

        self._packages = {}
        for item in os.listdir(source):
            yaml_path = os.path.join(source, item)
            if not os.path.isfile(yaml_path):
                continue
            if not yaml_path.endswith(".yml"):
                continue

            project = os.path.splitext(item)[0]

            try:
                with open(yaml_path, "r") as infile:
                    packages = yaml.safe_load(infile)
                    self._packages[project] = packages["packages"]
            except Exception as ex:
                raise Exception(
                    "Can't load packages for '{}': {}".format(project, ex))

    def expand_pattern(self, pattern):
        projects = Util.expand_pattern(pattern, self._packages, "project")

        # Some projects are internal implementation details and should
        # not be exposed to the user
        for project in ["base", "blacklist", "jenkins"]:
            if project in projects:
                projects.remove(project)

        return projects

    def get_mappings(self):
        return self._mappings

    def get_pip_mappings(self):
        return self._pip_mappings

    def get_packages(self, project):
        return self._packages[project]


class Application:

    def __init__(self):
        self._config = Config()
        self._inventory = Inventory()
        self._projects = Projects()

        self._native_arch = Util.get_native_arch()

        self._parser = argparse.ArgumentParser(
            conflict_handler="resolve",
            description="libvirt CI guest management tool",
        )

        self._parser.add_argument("--debug", action="store_true",
                                  help="display debugging information")

        subparsers = self._parser.add_subparsers(metavar="ACTION")
        subparsers.required = True

        def add_hosts_arg(parser):
            parser.add_argument(
                "hosts",
                help="list of hosts to act on (accepts globs)",
            )

        def add_projects_arg(parser):
            parser.add_argument(
                "projects",
                help="list of projects to consider (accepts globs)",
            )

        def add_gitrev_arg(parser):
            parser.add_argument(
                "-g", "--git-revision",
                help="git revision to build (remote/branch)",
            )

        def add_cross_arch_arg(parser):
            parser.add_argument(
                "-x", "--cross-arch",
                help="target architecture for cross compiler",
            )

        def add_wait_arg(parser):
            parser.add_argument(
                "-w", "--wait",
                help="wait for installation to complete",
                default=False,
                action="store_true",
            )

        installparser = subparsers.add_parser(
            "install", help="perform unattended host installation")
        installparser.set_defaults(func=self._action_install)

        add_hosts_arg(installparser)
        add_wait_arg(installparser)

        updateparser = subparsers.add_parser(
            "update", help="prepare hosts and keep them updated")
        updateparser.set_defaults(func=self._action_update)

        add_hosts_arg(updateparser)
        add_projects_arg(updateparser)
        add_gitrev_arg(updateparser)

        buildparser = subparsers.add_parser(
            "build", help="build projects on hosts")
        buildparser.set_defaults(func=self._action_build)

        add_hosts_arg(buildparser)
        add_projects_arg(buildparser)
        add_gitrev_arg(buildparser)

        hostsparser = subparsers.add_parser(
            "hosts", help="list all known hosts")
        hostsparser.set_defaults(func=self._action_hosts)

        projectsparser = subparsers.add_parser(
            "projects", help="list all known projects")
        projectsparser.set_defaults(func=self._action_projects)

        dockerfileparser = subparsers.add_parser(
            "dockerfile", help="generate Dockerfile (doesn't access the host)")
        dockerfileparser.set_defaults(func=self._action_dockerfile)

        add_hosts_arg(dockerfileparser)
        add_projects_arg(dockerfileparser)
        add_cross_arch_arg(dockerfileparser)

    def _execute_playbook(self, playbook, hosts, projects, git_revision):
        base = Util.get_base()

        flavor = self._config.get_flavor()
        vault_pass_file = self._config.get_vault_password_file()
        root_pass_file = self._config.get_root_password_file()

        ansible_hosts = ",".join(self._inventory.expand_pattern(hosts))
        selected_projects = self._projects.expand_pattern(projects)

        if git_revision is not None:
            tokens = git_revision.split("/")
            if len(tokens) < 2:
                raise Exception(
                    "Missing or invalid git revision '{}'".format(git_revision)
                )
            git_remote = tokens[0]
            git_branch = "/".join(tokens[1:])
        else:
            git_remote = "default"
            git_branch = "master"

        ansible_cfg_path = os.path.join(base, "ansible.cfg")
        playbook_base = os.path.join(base, "playbooks", playbook)
        playbook_path = os.path.join(playbook_base, "main.yml")

        extra_vars = json.dumps({
            "base": base,
            "playbook_base": playbook_base,
            "root_password_file": root_pass_file,
            "flavor": flavor,
            "selected_projects": selected_projects,
            "git_remote": git_remote,
            "git_branch": git_branch,
        })

        ansible_playbook = distutils.spawn.find_executable("ansible-playbook")
        if ansible_playbook is None:
            raise Exception("Cannot find ansible-playbook in $PATH")

        cmd = [
            ansible_playbook,
            "--limit", ansible_hosts,
            "--extra-vars", extra_vars,
        ]

        # Provide the vault password if available
        if vault_pass_file is not None:
            cmd += ["--vault-password-file", vault_pass_file]

        cmd += [playbook_path]

        # We need to point Ansible to the correct configuration file,
        # and for some reason this has to be done using the environment
        # rather than through the command line
        os.environ["ANSIBLE_CONFIG"] = ansible_cfg_path

        try:
            subprocess.check_call(cmd)
        except Exception as ex:
            raise Exception(
                "Failed to run {} on '{}': {}".format(playbook, hosts, ex))

    def _action_hosts(self, args):
        for host in self._inventory.expand_pattern("all"):
            print(host)

    def _action_projects(self, args):
        for project in self._projects.expand_pattern("all"):
            print(project)

    def _action_install(self, args):
        base = Util.get_base()

        flavor = self._config.get_flavor()

        for host in self._inventory.expand_pattern(args.hosts):
            facts = self._inventory.get_facts(host)

            # Both memory size and disk size are stored as GiB in the
            # inventory, but virt-install expects the disk size in GiB
            # and the memory size in *MiB*, so perform conversion here
            memory_arg = str(int(facts["install_memory_size"]) * 1024)

            vcpus_arg = str(facts["install_vcpus"])

            disk_arg = "size={},pool={},bus=virtio".format(
                facts["install_disk_size"],
                facts["install_storage_pool"],
            )
            network_arg = "network={},model=virtio".format(
                facts["install_network"],
            )

            # Different operating systems require different configuration
            # files for unattended installation to work, but some operating
            # systems simply don't support unattended installation at all
            if facts["os_name"] in ["Debian", "Ubuntu"]:
                install_config = "preseed.cfg"
            elif facts["os_name"] in ["CentOS", "Fedora"]:
                install_config = "kickstart.cfg"
            elif facts["os_name"] == "OpenSUSE":
                install_config = "autoinst.xml"
            else:
                raise Exception(
                    "Host {} doesn't support installation".format(host)
                )

            # Unattended install scripts are being generated on the fly, based
            # on the templates present in guests/configs/
            unattended_options = [
                "install_url",
            ]

            initrd_template = os.path.join(base, "configs", install_config)
            with open(initrd_template, 'r') as template:
                content = template.read()
                for option in unattended_options:
                    content = content.replace(
                        "{{ " + option + " }}",
                        facts[option]
                    )

                tempdir = tempfile.mkdtemp()
                initrd_inject = os.path.join(tempdir, install_config)

                with open(initrd_inject, "w") as inject:
                    inject.write(content)

            # preseed files must use a well-known name to be picked up by
            # d-i; for kickstart files, we can use whatever name we please
            # but we need to point anaconda in the right direction through
            # the 'ks' kernel parameter. We can use 'ks' unconditionally
            # for simplicity's sake, because distributions that don't use
            # kickstart for unattended installation will simply ignore it.
            # We do the same with the 'install' argument in order to
            # workaround a bug which causes old virt-install versions to
            # not pass the URL correctly when installing openSUSE guests
            extra_arg = "console=ttyS0 ks=file:/{} install={}".format(
                install_config,
                facts["install_url"],
            )

            virt_install = distutils.spawn.find_executable("virt-install")
            if virt_install is None:
                raise Exception("Cannot find virt-install in $PATH")

            cmd = [
                virt_install,
                "--name", host,
                "--location", facts["install_url"],
                "--virt-type", facts["install_virt_type"],
                "--arch", facts["install_arch"],
                "--machine", facts["install_machine"],
                "--cpu", facts["install_cpu_model"],
                "--vcpus", vcpus_arg,
                "--memory", memory_arg,
                "--disk", disk_arg,
                "--network", network_arg,
                "--graphics", "none",
                "--console", "pty",
                "--sound", "none",
                "--rng", "device=/dev/urandom,model=virtio",
                "--initrd-inject", initrd_inject,
                "--extra-args", extra_arg,
            ]

            if not args.wait:
                cmd.append("--noautoconsole")

            # Only configure autostart for the guest for the jenkins flavor
            if flavor == "jenkins":
                cmd += ["--autostart"]

            try:
                subprocess.check_call(cmd)
            except Exception as ex:
                raise Exception("Failed to install '{}': {}".format(host, ex))

            shutil.rmtree(tempdir, ignore_errors=True)

    def _action_update(self, args):
        self._execute_playbook("update", args.hosts, args.projects,
                               args.git_revision)

    def _action_build(self, args):
        self._execute_playbook("build", args.hosts, args.projects,
                               args.git_revision)

    def _get_openvz_repo(self):
        base = Util.get_base()
        repofile = os.path.join(base, "playbooks", "update", "templates", "openvz.repo.j2")
        with open(repofile, "r") as r:
            return r.read().rstrip()

    def _get_openvz_key(self):
        base = Util.get_base()
        keyfile = os.path.join(base, "playbooks", "update", "templates", "openvz.key")
        with open(keyfile, "r") as r:
            return r.read().rstrip()

    def _dockerfile_build_varmap(self, facts, mappings, pip_mappings, projects, cross_arch):
        if facts["package_format"] == "deb":
            varmap = self._dockerfile_build_varmap_deb(facts, mappings, pip_mappings, projects, cross_arch)
        if facts["package_format"] == "rpm":
            varmap = self._dockerfile_build_varmap_rpm(facts, mappings, pip_mappings, projects, cross_arch)

        varmap["package_manager"] = facts["package_manager"]
        varmap["cc"] = facts["cc"]
        varmap["ccache"] = facts["ccache"]
        varmap["make"] = facts["make"]
        varmap["ninja"] = facts["ninja"]
        varmap["python"] = facts["python"]

        if cross_arch:
            varmap["cross_abi"] = Util.native_arch_to_abi(cross_arch)

        return varmap

    def _dockerfile_build_varmap_deb(self, facts, mappings, pip_mappings, projects, cross_arch):
        package_format = facts["package_format"]
        package_manager = facts["package_manager"]
        os_name = facts["os_name"]
        os_version = facts["os_version"]
        os_full = os_name + os_version

        pkgs = {}
        cross_pkgs = {}
        pip_pkgs = {}
        base_keys = ["default", package_format, os_name, os_full]
        cross_keys = []
        if cross_arch:
            keys = base_keys + [cross_arch + "-" + k for k in base_keys]
            cross_keys = ["cross-policy-" + k for k in base_keys]
        else:
            keys = base_keys + [self._native_arch + "-" + k for k in base_keys]

        # We need to add the base project manually here: the standard
        # machinery hides it because it's an implementation detail
        for project in projects + ["base"]:
            for package in self._projects.get_packages(project):
                cross_policy = "native"
                for key in cross_keys:
                    if key in mappings[package]:
                        cross_policy = mappings[package][key]
                if cross_policy not in ["native", "foreign", "skip"]:
                    raise Exception(
                        "Unexpected cross arch policy {} for {}".format
                        (cross_policy, package))

                for key in keys:
                    if key in mappings[package]:
                        pkgs[package] = mappings[package][key]
                    if package in pip_mappings and key in pip_mappings[package]:
                        pip_pkgs[package] = pip_mappings[package][key]

                if package not in pkgs:
                    continue
                if package in pip_pkgs and pkgs[package] is not None:
                    del pip_pkgs[package]
                if cross_policy == "foreign" and pkgs[package] is not None:
                    cross_pkgs[package] = pkgs[package]
                if pkgs[package] is None or cross_policy in ["skip", "foreign"]:
                    del pkgs[package]

        pkg_align = " \\\n" + (" " * len("RUN " + package_manager + " "))
        pip_pkg_align = " \\\n" + (" " * len("RUN pip3 "))

        varmap = {}
        varmap["pkgs"] = pkg_align[1:] + pkg_align.join(sorted(set(pkgs.values())))

        if cross_arch:
            deb_arch = Util.native_arch_to_deb_arch(cross_arch)
            abi = Util.native_arch_to_abi(cross_arch)
            lib = Util.native_arch_to_lib(cross_arch)
            gcc = "gcc-" + abi
            varmap["cross_arch"] = deb_arch
            pkg_names = [p + ":" + deb_arch for p in cross_pkgs.values()]
            pkg_names.append(gcc)
            varmap["cross_pkgs"] = pkg_align[1:] + pkg_align.join(sorted(set(pkg_names)))
            varmap["cross_lib"] = lib

        if pip_pkgs:
            varmap["pip_pkgs"] = pip_pkg_align[1:] + pip_pkg_align.join(sorted(set(pip_pkgs.values())))

        return varmap

    def _dockerfile_build_varmap_rpm(self, facts, mappings, pip_mappings, projects, cross_arch):
        package_format = facts["package_format"]
        package_manager = facts["package_manager"]
        os_name = facts["os_name"]
        os_version = facts["os_version"]
        os_full = os_name + os_version

        pkgs = {}
        cross_pkgs = {}
        pip_pkgs = {}
        keys = ["default", package_format, os_name, os_full]

        # We need to add the base project manually here: the standard
        # machinery hides it because it's an implementation detail
        for project in projects + ["base"]:
            for package in self._projects.get_packages(project):
                for key in keys:
                    if key in mappings[package]:
                        pkgs[package] = mappings[package][key]
                    if package in pip_mappings and key in pip_mappings[package]:
                        pip_pkgs[package] = pip_mappings[package][key]

                if package not in pkgs:
                    continue
                if package in pip_pkgs and pkgs[package] is not None:
                    del pip_pkgs[package]
                if pkgs[package] is None:
                    del pkgs[package]

        if cross_arch:
            cross_projects = []
            for project in projects:
                cross_project = project + "+" + cross_arch
                if cross_project in facts["projects"]:
                    cross_projects.extend([cross_project])

            for project in cross_projects:
                for package in self._projects.get_packages(project):
                    for key in keys:
                        if key in mappings[package]:
                            cross_pkgs[package] = mappings[package][key]

                    if package not in cross_pkgs:
                        continue
                    if cross_pkgs[package] is None:
                        del cross_pkgs[package]

        pkg_align = " \\\n" + (" " * len("RUN " + package_manager + " "))
        pip_pkg_align = " \\\n" + (" " * len("RUN pip3 "))

        varmap = {}
        varmap["pkgs"] = pkg_align[1:] + pkg_align.join(sorted(set(pkgs.values())))

        if cross_arch:
            varmap["cross_pkgs"] = pkg_align[1:] + pkg_align.join(sorted(set(cross_pkgs.values())))

        if pip_pkgs:
            varmap["pip_pkgs"] = pip_pkg_align[1:] + pip_pkg_align.join(sorted(set(pip_pkgs.values())))

        return varmap

    def _dockerfile_format(self, facts, cross_arch, varmap):
        package_format = facts["package_format"]
        package_manager = facts["package_manager"]
        os_name = facts["os_name"]
        os_version = facts["os_version"]

        print("FROM {}".format(facts["docker_base"]))

        commands = []

        if package_format == "deb":
            commands.extend([
                "export DEBIAN_FRONTEND=noninteractive",
                "{package_manager} update",
                "{package_manager} dist-upgrade -y",
                "{package_manager} install --no-install-recommends -y {pkgs}",
                "{package_manager} autoremove -y",
                "{package_manager} autoclean -y",
                "sed -Ei 's,^# (en_US\\.UTF-8 .*)$,\\1,' /etc/locale.gen",
                "dpkg-reconfigure locales",
            ])
        elif package_format == "rpm":
            # Rawhide needs this because the keys used to sign packages are
            # cycled from time to time
            if os_name == "Fedora" and os_version == "Rawhide":
                commands.extend([
                    "{package_manager} update -y --nogpgcheck fedora-gpg-keys"
                ])

            if os_name == "CentOS":
                # Starting with CentOS 8, most -devel packages are shipped in
                # the PowerTools repository, which is not enabled by default
                if os_version != "7":
                    commands.extend([
                        "{package_manager} install 'dnf-command(config-manager)' -y",
                        "{package_manager} config-manager --set-enabled PowerTools -y",
                    ])

                # VZ development packages are only available for CentOS/RHEL-7
                # right now and need a 3rd party repository enabled
                if os_version == "7":
                    repo = self._get_openvz_repo()
                    varmap["vzrepo"] = "\\n\\\n".join(repo.split("\n"))
                    key = self._get_openvz_key()
                    varmap["vzkey"] = "\\n\\\n".join(key.split("\n"))

                    commands.extend([
                        "echo -e '{vzrepo}' > /etc/yum.repos.d/openvz.repo",
                        "echo -e '{vzkey}' > /etc/pki/rpm-gpg/RPM-GPG-KEY-OpenVZ",
                        "rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-OpenVZ"
                    ])

                # Some of the packages we need are not part of CentOS proper
                # and are only available through EPEL
                commands.extend([
                    "{package_manager} install -y epel-release",
                ])

            commands.extend([
                "{package_manager} update -y",
                "{package_manager} install -y {pkgs}",
            ])

            # openSUSE doesn't seem to have a convenient way to remove all
            # unnecessary packages, but CentOS and Fedora do
            if os_name == "OpenSUSE":
                commands.extend([
                    "{package_manager} clean --all",
                ])
            else:
                commands.extend([
                    "{package_manager} autoremove -y",
                    "{package_manager} clean all -y",
                ])

        commands.extend([
            "mkdir -p /usr/libexec/ccache-wrappers",
        ])

        if cross_arch:
            commands.extend([
                "ln -s {ccache} /usr/libexec/ccache-wrappers/{cross_abi}-cc",
                "ln -s {ccache} /usr/libexec/ccache-wrappers/{cross_abi}-$(basename {cc})",
            ])
        else:
            commands.extend([
                "ln -s {ccache} /usr/libexec/ccache-wrappers/cc",
                "ln -s {ccache} /usr/libexec/ccache-wrappers/$(basename {cc})",
            ])

        script = "\nRUN " + (" && \\\n    ".join(commands)) + "\n"
        sys.stdout.write(script.format(**varmap))

        if cross_arch:
            cross_commands = []

            # Intentionally a separate RUN command from the above
            # so that the common packages of all cross-built images
            # share a Docker image layer.
            if package_format == "deb":
                cross_commands.extend([
                    "export DEBIAN_FRONTEND=noninteractive",
                    "dpkg --add-architecture {cross_arch}",
                    "{package_manager} update",
                    "{package_manager} dist-upgrade -y",
                    "{package_manager} install --no-install-recommends -y dpkg-dev",
                    "{package_manager} install --no-install-recommends -y {cross_pkgs}",
                    "{package_manager} autoremove -y",
                    "{package_manager} autoclean -y",
                ])
            elif package_format == "rpm":
                cross_commands.extend([
                    "{package_manager} install -y {cross_pkgs}",
                    "{package_manager} clean all -y",
                ])

            cross_script = "\nRUN " + (" && \\\n    ".join(cross_commands)) + "\n"
            sys.stdout.write(cross_script.format(**varmap))

        if "pip_pkgs" in varmap:
            sys.stdout.write(textwrap.dedent("""
                RUN pip3 install {pip_pkgs}
            """).format(**varmap))

        sys.stdout.write(textwrap.dedent("""
            ENV LANG "en_US.UTF-8"

            ENV MAKE "{make}"
            ENV NINJA "{ninja}"
            ENV PYTHON "{python}"

            ENV CCACHE_WRAPPERSDIR "/usr/libexec/ccache-wrappers"
        """).format(**varmap))

        if cross_arch:
            sys.stdout.write(textwrap.dedent("""
                ENV ABI "{cross_abi}"
                ENV CONFIGURE_OPTS "--host={cross_abi}"
            """).format(**varmap))

    def _action_dockerfile(self, args):
        mappings = self._projects.get_mappings()
        pip_mappings = self._projects.get_pip_mappings()

        hosts = self._inventory.expand_pattern(args.hosts)
        if len(hosts) > 1:
            raise Exception("Can't generate Dockerfile for multiple hosts")
        host = hosts[0]

        facts = self._inventory.get_facts(host)
        package_format = facts["package_format"]
        package_manager = facts["package_manager"]
        os_name = facts["os_name"]
        os_version = facts["os_version"]
        os_full = os_name + os_version
        cross_arch = args.cross_arch

        if package_format not in ["deb", "rpm"]:
            raise Exception("Host {} doesn't support Dockerfiles".format(host))
        if cross_arch:
            if os_name not in ["Debian", "Fedora"]:
                raise Exception("Cannot cross compile on {}".format(os_name))
            if os_name == "Debian" and cross_arch.startswith("mingw"):
                raise Exception(
                    "Cannot cross compile for {} on {}".format(
                        cross_arch,
                        os_name,
                    )
                )
            if os_name == "Fedora" and not cross_arch.startswith("mingw"):
                raise Exception(
                    "Cannot cross compile for {} on {}".format(
                        cross_arch,
                        os_name,
                    )
                )
            if cross_arch == self._native_arch:
                raise Exception("Cross arch {} should differ from native {}".
                                format(cross_arch, self._native_arch))

        projects = self._projects.expand_pattern(args.projects)
        for project in projects:
            if project.rfind("+mingw") >= 0:
                raise Exception("Obsolete syntax, please use --cross-arch")
            if project not in facts["projects"]:
                raise Exception(
                    "Host {} doesn't support project {}".format(
                        host,
                        project,
                    )
                )

        varmap = self._dockerfile_build_varmap(facts, mappings, pip_mappings, projects, cross_arch)
        self._dockerfile_format(facts, cross_arch, varmap)

    def run(self):
        args = self._parser.parse_args()
        if args.debug:
            args.func(args)
        else:
            try:
                args.func(args)
            except Exception as err:
                sys.stderr.write("{}: {}\n".format(sys.argv[0], err))
                sys.exit(1)


if __name__ == "__main__":
    Application().run()
